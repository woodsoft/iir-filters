## 0.1.0 (2024-02-07)

### added (1 change)

- [feat: Initialize Repository](woodsoft/plugin-template@6b0a46936984b768f7e17d1f88e29dc22b7b8f14) ([merge request](woodsoft/plugin-template!5))
