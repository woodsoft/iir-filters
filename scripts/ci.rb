#!/usr/bin/ruby

require 'colorize'
require 'net/http'
require 'uri'

PROJECT_NAME = "iir-filters"

def runcmd(cmd)
  puts cmd.colorize(:color => :green, :mode => :bold)
  ret = system(cmd)

  # Exit on system command failure
  if !ret
    exit(ret)
  end
end

def version
  # Install dependencies
  runcmd("apt update")
  runcmd("DEBIAN_FRONTEND=noninteractive apt -y install pipx git")
  runcmd("pipx install vernum")

  latest_version = `git tag --list --merged HEAD | tail -n 1`.strip()

  if /^feat:.*$/.match(ENV["CI_COMMIT_MESSAGE"])
    system("~/.local/bin/vernum -s #{latest_version} minor > .version")
  elsif /^rel:.*$/.match(ENV["CI_COMMIT_MESSAGE"])
    system("~/.local/bin/vernum -s #{latest_version} major > .version")
  else
    system("~/.local/bin/vernum -s #{latest_version} patch > .version")
  end
end

def build
  # Install dependencies
  cmd = "apt update && DEBIAN_FRONTEND=noninteractive apt install -y"
  cmd += " cmake make autoconf zip libxinerama-dev libxcursor-dev"
  cmd += " xorg-dev libglu1-mesa-dev pkg-config git curl unzip tar g++ python3"
  runcmd(cmd)

  runcmd("git submodule update --init --recursive")
  runcmd("./external/vcpkg/bootstrap-vcpkg.sh")
  runcmd("cmake -S . -B build -DCMAKE_TOOLCHAIN_FILE=./external/vcpkg/scripts/buildsystems/vcpkg.cmake")
  runcmd("cmake --build build")
end

def test
  # Install dependencies
  cmd = "apt update && DEBIAN_FRONTEND=noninteractive apt install -y"
  cmd += " curl git rustc cargo"
  runcmd(cmd)

  runcmd("git clone https://github.com/free-audio/clap-validator.git")
  runcmd("cargo run --manifest-path clap-validator/Cargo.toml --release -- validate build/#{PROJECT_NAME}.clap")
end

def publish
  branch = ENV["CI_COMMIT_BRANCH"]
  target_branch = ENV["CI_MERGE_REQUEST_TARGET_BRANCH_NAME"]
  project_name = ENV["CI_PROJECT_NAME"]
  pipeline_iid = ENV["CI_PIPELINE_IID"]
  version = `cat .version`.strip()
  pub_name = ""

  if branch == "main" or target_branch == "main"
    pub_name = "#{project_name}-v#{version}"
  else
    pub_name = "#{project_name}-v#{version}-#{branch}.#{pipeline_iid}"
  end

  runcmd("mkdir publish")
  runcmd("cp build/#{project_name}.clap publish/#{pub_name}.clap")
  runcmd("ls -l publish/")
end

def release
  commit_msg = ENV["CI_COMMIT_MESSAGE"]
  if !/^fix:.*$/.match(commit_msg) and !/^feat:.*$/.match(commit_msg) and !/^rel:.*$/.match(commit_msg)
    puts "Nothing to do for release."
    return
  end

  version = `cat .version`.strip()
  token = ENV["GITLAB_TOKEN"]
  branch = ENV["CI_COMMIT_BRANCH"]
  api_v4_url = ENV["CI_API_V4_URL"]
  project_id = ENV["CI_PROJECT_ID"]
  tag = "v#{version}"
  msg = "Release #{tag}"

  # Install dependencies
  runcmd("apt update && DEBIAN_FRONTEND=noninteractive apt install -y git curl")

  runcmd("git config --global user.name \"#{ENV["GITLAB_USER_NAME"]}\"")
  runcmd("git config --global user.email \"#{ENV["GITLAB_USER_EMAIL"]}\"")

  # TODO push plugin to storage i.e. artifactory, S3, etc...
  runcmd("git tag -a #{tag} -m \"#{msg}\"")
  runcmd("git push --tags https://woodsoft:#{token}@#{ENV["CI_SERVER_HOST"]}/#{ENV["CI_PROJECT_PATH"]}.git HEAD:#{branch}")

  # Generate Release
  uri = "#{api_v4_url}/projects/#{project_id}/releases"
  curl_cmd = "curl --header 'Content-Type: application/json' --header \"PRIVATE-TOKEN: #{token}\""
  curl_cmd += " --data '{ \"name\": \"Release #{tag}\", \"tag_name\": \"#{tag}\", \"description\": \"Release #{tag}\"}'"
  curl_cmd += " -X POST \"#{uri}\""
  runcmd(curl_cmd)

  # Update CHANGELOG
  uri = "#{api_v4_url}/projects/#{project_id}/repository/changelog?version=#{version}"
  runcmd("curl -X POST --header \"PRIVATE-TOKEN: #{token}\" \"#{uri}\"")
end

def main
  case ARGV[0]
  when "version"
    version
  when "build"
    build
  when "test"
    test
  when "publish"
    publish
  when "release"
    release
  end
end

main
