# IIR Filters

## Getting Started

```
git clone https://gitlab.com/woodsoft/iir-filters.git
cd iir-filters
./init.sh
```

This will generate the `IIR Filters` CLAP plugin and copy it to `~/.clap/`.

## Credits
The `AudioFilter` and `Biquad` classes are modeled after classes of the same names found in
"Designing Audio Effect Plugins in C++" by [Will C. Pirkle](https://www.willpirkle.com/).

*NOTE: Only Ubuntu is supported at this time.*