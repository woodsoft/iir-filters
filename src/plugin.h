#ifndef PLUGIN_H
#define PLUGIN_H

#include <pthread.h>

#include <clap/clap.h>

#include <audio-ports.h>
#include <timer.h>
#include <params.h>
#include <state.h>
#include <gui.h>

#include <dsp.h>

class Plugin {
public:
    Plugin(const char *plugin_path, const clap_host_t *host);

    bool init();
    void destroy();
    bool activate(double sample_rate, uint32_t min_nframes, uint32_t max_nframes);
    void deactivate();
    bool startProcessing();
    void stopProcessing();
    void reset();
    clap_process_status process(const clap_process_t *process);
    const void* getExtension(const char *id);
    void onMainThread();

    const clap_plugin_t* clapPlugin();

    // Extension getters
    AudioPorts *getAudioPorts();
    Timer *getTimer();
    Params *getParams();
    State *getState();
    GUI *getGUI();

    DSP *getDSP();

    void processEvent(const clap_event_header_t *event);

    void startTimer();
    void stopTimer();

    // Timer handler
    void onTimer(clap_id timer_id);

    static const clap_plugin_descriptor_t* descriptor() {
        return &DESCRIPTOR;
    }

private:
    double sample_rate;
    uint32_t min_nframes, max_nframes;
    clap_plugin_t plugin;
    const clap_host_t *host;
    const char *plugin_path;
    bool active, processing;

    clap_id timer_id;

    // Extensions
    AudioPorts *audio_ports;
    Timer *timer;
    Params *params;
    State *state;
    GUI *gui;

    DSP *dsp;

    void initClapPlugin();

    static constexpr clap_plugin_descriptor_t DESCRIPTOR = {
        .clap_version   = CLAP_VERSION_INIT,
        .id             = "woodsoft.IIRFilters",
        .name           = "IIR Filters",
        .vendor         = "woodsoft",
        .url            = "https://woodsoft.io",
        .manual_url     = "https://woodsoft.io",
        .support_url    = "https://woodosoft.io",
        .version        = "0.0.0",
        .description    = "woodsoft IIR Filters",

        .features = (const char *[]) {
            CLAP_PLUGIN_FEATURE_MONO,
            CLAP_PLUGIN_FEATURE_AUDIO_EFFECT,
            CLAP_PLUGIN_FEATURE_FILTER,
            NULL,
        },
    };
};

#endif // PLUGIN_H
