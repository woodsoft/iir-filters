#include <cstdio>

#include <audio-ports.h>
#include <plugin.h>

AudioPorts::AudioPorts() {
    initClapExtension();
}

AudioPorts::~AudioPorts() {

}

uint32_t AudioPorts::count(bool is_input) {
    return 1;
}

bool AudioPorts::get(uint32_t index, bool is_input, clap_audio_port_info_t *info) {
    if (index > 0) return false;
    info->id = 0;
    info->channel_count = 2;
    info->flags = CLAP_AUDIO_PORT_IS_MAIN;
    info->port_type = CLAP_PORT_STEREO;
    info->in_place_pair = CLAP_INVALID_ID;
    snprintf(info->name, sizeof(info->name), "%s", "Audio Output");
    return true;
}

const void *AudioPorts::clapExtension() {
    return &audio_ports;
}

void AudioPorts::initClapExtension() {
    audio_ports = {
        .count = [] (const clap_plugin_t *cplugin, bool is_input) -> uint32_t {
            Plugin *plugin = (Plugin *) cplugin->plugin_data;
            AudioPorts *audio_ports = plugin->getAudioPorts();
            return audio_ports->count(is_input);
        },

        .get = [] (const clap_plugin_t *cplugin, uint32_t index, bool is_input, clap_audio_port_info_t *info) -> bool {
            Plugin *plugin = (Plugin *) cplugin->plugin_data;
            AudioPorts *audio_ports = plugin->getAudioPorts();
            return audio_ports->get(index, is_input, info);
        },
    };
}
