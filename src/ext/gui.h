#ifndef GUI_H
#define GUI_H

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#define GLFW_EXPOSE_NATIVE_X11
#include <GLFW/glfw3native.h>

#include <imgui.h>
#include <imgui_internal.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>
#include <imgui-knobs.h>

#include <clap/clap.h>

#include <params.h>
#include <dsp.h>

#define GUI_WIDTH   400
#define GUI_HEIGHT  400
#define GUI_API CLAP_WINDOW_API_X11

class GUI {
public:
    GUI(Params *params);
    ~GUI();

    bool isAPISupported(const char *api, bool is_floating);
    bool getPreferredAPI(const char **api, bool *is_floating);
    bool create(const char *api, bool is_floating);
    void destroy();
    bool setScale(double scale);
    bool getSize(uint32_t *width, uint32_t *height);
    bool canResize();
    bool getResizeHints(clap_gui_resize_hints_t *hints);
    bool adjustSize(uint32_t *width, uint32_t *height);
    bool setSize(uint32_t width, uint32_t height);
    bool setParent(const clap_window_t *window);
    bool setTransient(const clap_window_t *window);
    void suggestTitle(const char *title);
    bool show(DSP *dsp);
    bool hide();

    const void *clapExtension();
private:
    Params *params;
    clap_plugin_gui_t gui_ext;

    GLFWwindow *window;
    Window parent_window;
    Display *xdisplay;
    const char *glsl_version;

    uint32_t height, width;
    double scale;

    void newFrame();
    void drawVolume();
    void drawAudioFilter(DSP *dsp);
    void drawBiquad(DSP *dsp);
    void render();

    void initClapExtension();
};

#endif // GUI_H
