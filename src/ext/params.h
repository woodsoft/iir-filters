#ifndef PARAMS_H
#define PARAMS_H

#include <array>

#include <clap/clap.h>
#include <pthread.h>

enum {
    P_VOLUME,
    P_BIQUAD_ALGO,
    P_AUDIO_FILTER_ALGO,
    P_AUDIO_FILTER_FC,
    P_AUDIO_FILTER_Q,
    P_AUDIO_FILTER_BOOST_CUT_DB,
    P_COUNT,
};

typedef struct {
    float value;
    float main_value;
    bool changed;
    bool main_changed;
} param_data_t;

class Params {
public:
    Params();
    ~Params();

    uint32_t count();
    bool getInfo(uint32_t index, clap_param_info_t *info);
    bool getValue(clap_id id, double *value);
    bool valueToText(clap_id id, double value, char *display, uint32_t size);
    bool textToValue(clap_id id, const char *display, double *value);
    void flush(const clap_input_events_t *in, const clap_output_events_t *out);

    void processEvent(const clap_event_header_t *event);

    void syncMainToAudio(const clap_output_events_t *out);
    bool syncAudioToMain();

    bool save(const clap_ostream_t *stream);
    bool load(const clap_istream_t *stream);

    param_data_t *getParamData(clap_id id);

    const void *clapExtension();
private:
    pthread_mutex_t params_mutex;
    clap_plugin_params_t params_ext;

    std::array<param_data_t, P_COUNT> param_data;

    void initClapExtension();
};

#endif // PARAMS_H
