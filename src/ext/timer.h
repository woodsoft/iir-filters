#ifndef TIMER_H
#define TIMER_H

#include <unordered_map>

#include <clap/clap.h>

#define TIMER_MS 30

enum {
    TIMER_GUI,
    TIMER_COUNT
};

class Timer {
public:
    Timer(const clap_host_t *host);
    ~Timer();

    bool createTimer(uint32_t period_ms, clap_id *timer_id);
    void destroyTimer(clap_id *timer_id);

    const void *clapExtension();
private:
    clap_plugin_timer_support_t timer_ext;
    const clap_host_timer_support_t *timer_support;
    const clap_host_t *host;

    void initClapExtension();
};

#endif // TIMER_H
