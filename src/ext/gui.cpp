#include <gui.h>
#include <plugin.h>

static void glfwErrorCallback(int error, const char *description) {
    fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}

static GUI *getGUI(const clap_plugin_t *cplugin) {
    Plugin *plugin = (Plugin *) cplugin->plugin_data;
    return plugin->getGUI();
}

GUI::GUI(Params *params) :
    params(params),
    glsl_version("#version 130") {
    fprintf(stderr, "GUI::GUI()\n");
    this->initClapExtension();
}

GUI::~GUI() {}

bool GUI::isAPISupported(const char *api, bool is_floating) {
    fprintf(stderr, "GUI::isAPISupported()\n");
    return !strcmp(api, GUI_API) && !is_floating;
}

bool GUI::getPreferredAPI(const char **api, bool *is_floating) {
    fprintf(stderr, "GUI::getPreferredAPI()\n");
    *api = GUI_API;
    *is_floating = false;
    return true;
}

bool GUI::create(const char *api, bool is_floating) {
    this->height = GUI_HEIGHT;
    this->width = GUI_WIDTH;
    this->xdisplay = (Display *) XOpenDisplay(NULL);

    glfwSetErrorCallback(glfwErrorCallback);
    if (!glfwInit()) {
        fprintf(stderr, "Error initializing GLFW.\n");
        return false;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    this->window = glfwCreateWindow(GUI_WIDTH, GUI_HEIGHT, "Woodsoft Plugin Template", NULL, NULL);
    if (this->window == NULL) {
        fprintf(stderr, "Error creating GLFW Window.\n");
        return false;
    }
    glfwMakeContextCurrent(this->window);
    glfwSwapInterval(1);

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO &io = ImGui::GetIO();
    ImGui_ImplOpenGL3_Init(this->glsl_version);
    ImGui_ImplGlfw_InitForOpenGL(this->window, true);
    ImGui::StyleColorsDark();

    fprintf(stderr, "GUI::create(), this->xdisplay: %p\n", this->xdisplay);
    return true;
}

void GUI::destroy() {
    fprintf(stderr, "GUI::destroy()\n");
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    this->window = nullptr;
}

bool GUI::setScale(double scale) {
    fprintf(stderr, "GUI::setScale()\n");
    this->scale = scale;
    return true;
}

bool GUI::getSize(uint32_t *width, uint32_t *height) {
    fprintf(stderr, "GUI::getSize()\n");
    *width = this->width;
    *height = this->height;
    return true;
}

bool GUI::canResize() {
    fprintf(stderr, "GUI::canResize()\n");
    return true;
}

bool GUI::getResizeHints(clap_gui_resize_hints_t *hints) {
    fprintf(stderr, "GUI::getResizeHints()\n");
    hints->can_resize_horizontally = true;
    hints->can_resize_vertically = true;
    hints->preserve_aspect_ratio = true;
    hints->aspect_ratio_width = 1;
    hints->aspect_ratio_height = 1;

    return true;
}

bool GUI::adjustSize(uint32_t *width, uint32_t *height) {
    fprintf(stderr, "GUI::adjustSize()\n");
    return this->getSize(width, height);
}

bool GUI::setSize(uint32_t width, uint32_t height) {
    fprintf(stderr, "GUI::setSize()\n");
    this->width = width;
    this->height = height;

    return true;
}

bool GUI::setParent(const clap_window_t *window) {
    fprintf(stderr, "GUI::setParent()\n");
    this->parent_window = (Window) window->x11;

    XReparentWindow(this->xdisplay, (Window) glfwGetX11Window(this->window), this->parent_window, 0, 0);
    XFlush(this->xdisplay);

    return true;
}

bool GUI::setTransient(const clap_window_t *window) {
    fprintf(stderr, "GUI::setTransient()\n");
    return false;
}

void GUI::suggestTitle(const char *title) {
    fprintf(stderr, "GUI::suggestTitle()\n");
}

bool GUI::show(DSP *dsp) {
    this->params->syncAudioToMain();
    this->newFrame();

    ImGui::Begin("Woodsoft Plugin Template", nullptr, ImGuiWindowFlags_NoDecoration);
    drawVolume();
    drawBiquad(dsp);
    drawAudioFilter(dsp);
    ImGui::End();

    this->render();
    return true;
}

bool GUI::hide() {
    fprintf(stderr, "GUI::hide()\n");
    return true;
}

const void *GUI::clapExtension() {
    fprintf(stderr, "GUI::clapExtension()\n");
    return &gui_ext;
}

void GUI::newFrame() {
    glfwPollEvents();

    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    XWindowAttributes xwa;
    XGetWindowAttributes(this->xdisplay, this->parent_window, &xwa);
    Window xroot = DefaultRootWindow(this->xdisplay), xchild = 0;

    if (xroot) {
        XTranslateCoordinates(this->xdisplay, this->parent_window, xroot, 0, 0, &xwa.x, &xwa.y, &xchild);
    }

    ImGui::SetNextWindowPos(ImVec2(0, 0));
    ImGui::SetNextWindowSize(ImVec2(xwa.width, xwa.height));
}

void GUI::drawVolume() {
    param_data_t *volume_data = params->getParamData(P_VOLUME);
    clap_param_info_t param_info = {};
    params->getInfo(P_VOLUME, &param_info);
    if (ImGuiKnobs::Knob("Volume", &volume_data->value, param_info.min_value, param_info.max_value, 0.1f, "%.1fdb", ImGuiKnobVariant_Tick)) {
        volume_data->changed = true;
    }
}

void GUI::drawAudioFilter(DSP *dsp) {
    AudioFilter *audio_filter = dsp->getAudioFilter();
    clap_param_info_t param_info = {};

    param_data_t *fc_data = params->getParamData(P_AUDIO_FILTER_FC);
    params->getInfo(P_AUDIO_FILTER_FC, &param_info);
    if (ImGuiKnobs::Knob(param_info.name, &fc_data->value, param_info.min_value, param_info.max_value, 10.0f, "%.1fdB", ImGuiKnobVariant_Tick)) {
        AudioFilterParameters audio_filter_params = audio_filter->getParameters();
        audio_filter_params.fc = fc_data->value;
        audio_filter->setParameters(audio_filter_params);

        fc_data->changed = true;
    }

    ImGui::SameLine();

    param_data_t *q_data = params->getParamData(P_AUDIO_FILTER_Q);
    params->getInfo(P_AUDIO_FILTER_Q, &param_info);
    if (ImGuiKnobs::Knob(param_info.name, &q_data->value, param_info.min_value, param_info.max_value, 0.1f, "%.1fdB", ImGuiKnobVariant_Tick)) {
        AudioFilterParameters audio_filter_params = audio_filter->getParameters();
        audio_filter_params.Q = q_data->value;
        audio_filter->setParameters(audio_filter_params);

        q_data->changed = true;
    }

    ImGui::SameLine();

    param_data_t *boost_cut_data = params->getParamData(P_AUDIO_FILTER_BOOST_CUT_DB);
    params->getInfo(P_AUDIO_FILTER_BOOST_CUT_DB, &param_info);
    if (ImGuiKnobs::Knob(param_info.name, &boost_cut_data->value, param_info.min_value, param_info.max_value, 0.1f, "%.1fdB", ImGuiKnobVariant_Tick)) {
        AudioFilterParameters audio_filter_params = audio_filter->getParameters();
        audio_filter_params.boostcut_db = boost_cut_data->value;
        audio_filter->setParameters(audio_filter_params);

        boost_cut_data->changed = true;
    }

    param_data_t *algo_data = params->getParamData(P_AUDIO_FILTER_ALGO);
    const char *filter_algorithms[] = {
        "LPF1P", "LPF1", "HPF1", "LPF2", "HPF2", "BPF2", "BSF2",
        "ButterLPF2", "ButterHPF2", "ButterBPF2", "ButterBSF2",
        "MMALPF2", "MMALPF2B",
        "LowShelf", "HiShelf", "NCQParaEQ", "CQParaEQ",
        "LWRLPF2", "LWRHPF2", "APF1", "APF2",
        "ResonA", "ResonB",
        "MatchLP2A", "MatchLP2B", "MatchBP2A", "MatchBP2B",
        "ImpInvLP1", "ImpInvLP2",
    };
    static int filter_algorithm_selection = 0;
    if (ImGui::Combo("Filter Algorithm", &filter_algorithm_selection, filter_algorithms, IM_ARRAYSIZE(filter_algorithms))) {
        AudioFilterParameters audio_filter_params = audio_filter->getParameters();
        audio_filter_params.algorithm = (FilterAlgorithm) filter_algorithm_selection;
        audio_filter->setParameters(audio_filter_params);
        algo_data->value = (double) filter_algorithm_selection;
        algo_data->changed = true;
    }
}

void GUI::drawBiquad(DSP *dsp) {
    const char *biquad_algorithms[] = {"Direct", "Canonical", "Transpose Direct", "Transpose Canonical"};
    static int choice = 0;
    param_data_t *biquad_data = params->getParamData(P_BIQUAD_ALGO);

    if (ImGui::Combo("Biquad Algorithm", &choice, biquad_algorithms, IM_ARRAYSIZE(biquad_algorithms))) {
        Biquad *biquad = dsp->getBiquad();
        BiquadParameters biquad_params = biquad->getParameters();
        switch ((BiquadAlgorithm) choice) {
        case BiquadAlgorithm::Direct:
            biquad_params.biquad_algorithm = BiquadAlgorithm::Direct;
            break;
        case BiquadAlgorithm::Canonical:
            biquad_params.biquad_algorithm = BiquadAlgorithm::Canonical;
            break;
        case BiquadAlgorithm::TransposeDirect:
            biquad_params.biquad_algorithm = BiquadAlgorithm::TransposeDirect;
            break;
        case BiquadAlgorithm::TransposeCanonical:
            biquad_params.biquad_algorithm = BiquadAlgorithm::TransposeCanonical;
            break;
        }
        biquad->setParameters(biquad_params);
        biquad_data->value = (double) choice;
        biquad_data->changed = true;
    }
}

void GUI::render() {
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    glfwSwapBuffers(this->window);

    XReparentWindow(this->xdisplay, (Window) glfwGetX11Window(this->window), this->parent_window, 0, 0);
    XFlush(this->xdisplay);
}

void GUI::initClapExtension() {
    fprintf(stderr, "GUI::initClapExtension()\n");
    gui_ext = {
        .is_api_supported = [] (const clap_plugin_t *cplugin, const char *api, bool is_floating) -> bool {
            return getGUI(cplugin)->isAPISupported(api, is_floating);
        },

        .get_preferred_api = [] (const clap_plugin_t *cplugin, const char **api, bool *is_floating) -> bool {
            return getGUI(cplugin)->getPreferredAPI(api, is_floating);
        },

        .create = [] (const clap_plugin_t *cplugin, const char *api, bool is_floating) -> bool {
            Plugin *plugin = (Plugin *) cplugin->plugin_data;

            bool result = getGUI(cplugin)->create(api, is_floating);
            plugin->startTimer();

            return result;
        },

        .destroy = [] (const clap_plugin_t *cplugin) {
            Plugin *plugin = (Plugin *) cplugin->plugin_data;
            plugin->stopTimer();
            getGUI(cplugin)->destroy();
        },

        .set_scale = [] (const clap_plugin_t *cplugin, double scale) -> bool {
            return getGUI(cplugin)->setScale(scale);
        },

        .get_size = [] (const clap_plugin_t *cplugin, uint32_t *width, uint32_t *height) -> bool {
            return getGUI(cplugin)->getSize(width, height);
        },
        
        .can_resize = [] (const clap_plugin_t *cplugin) -> bool {
            return getGUI(cplugin)->canResize();
        },

        .get_resize_hints = [] (const clap_plugin_t *cplugin, clap_gui_resize_hints_t *hints) -> bool {
            return getGUI(cplugin)->getResizeHints(hints);
        },

        .adjust_size = [] (const clap_plugin_t *cplugin, uint32_t *width, uint32_t *height) -> bool {
            return getGUI(cplugin)->adjustSize(width, height);
        },

        .set_size = [] (const clap_plugin_t *cplugin, uint32_t width, uint32_t height) -> bool {
            return getGUI(cplugin)->setSize(width, height);
        },

        .set_parent = [] (const clap_plugin_t *cplugin, const clap_window_t *window) -> bool {
            return getGUI(cplugin)->setParent(window);
        },

        .set_transient = [] (const clap_plugin_t *cplugin, const clap_window_t *window) -> bool {
            return getGUI(cplugin)->setTransient(window);
        },

        .suggest_title = [] (const clap_plugin_t *cplugin, const char *title) {
            getGUI(cplugin)->suggestTitle(title);
        },

        .show = [] (const clap_plugin_t *cplugin) -> bool {
            Plugin *plugin = (Plugin *) cplugin->plugin_data;
            return plugin->getGUI()->show(plugin->getDSP());
        },

        .hide = [] (const clap_plugin_t *cplugin) -> bool {
            return getGUI(cplugin)->hide();
        },
    };
}
