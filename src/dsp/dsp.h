#ifndef __DSP_H_
#define __DSP_H_

#include <params.h>
#include <audio-filter.h>
#include <biquad.h>

class DSP {
public:
    DSP(Params *params, double sample_rate);
    ~DSP();

    double process(double in);

    AudioFilter *getAudioFilter();
    Biquad *getBiquad();

private:
    Params *params;
    double sample_rate;

    AudioFilter *audio_filter;
    Biquad *biquad;

    double volume;

    void updateVolume();
};

#endif // __DSP_H_
