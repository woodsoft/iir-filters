#include <cstdio>
#include <cstring>

#include <biquad.h>

Biquad::Biquad(double sample_rate) :
    sample_rate(sample_rate) {
    biquad_parameters.biquad_algorithm = BiquadAlgorithm::Direct;
    memset(&coeffs, 0, sizeof(coeffs[a0]) * coeffs.size());
}

Biquad::~Biquad() {}

BiquadParameters Biquad::getParameters() {
    return biquad_parameters;
}

void Biquad::setParameters(BiquadParameters params) {
    biquad_parameters = params;
}

bool Biquad::reset(double sample_rate) {
    this->sample_rate = sample_rate;
    memset(&coeffs, 0, sizeof(coeffs[a0]) * coeffs.size());
    memset(&state, 0, sizeof(state[x_z1]) * state.size());

    return true;
}

double Biquad::process(double xn) {
    double yn = xn;

    switch (biquad_parameters.biquad_algorithm) {
    case BiquadAlgorithm::Direct:
        yn = processDirect(xn);
        break;
    case BiquadAlgorithm::Canonical:
        yn = processCanonical(xn);
        break;
    case BiquadAlgorithm::TransposeDirect:
        yn = processTransposeDirect(xn);
        break;
    case BiquadAlgorithm::TransposeCanonical:
        yn = processTransposeCanonical(xn);
        break;
    }

    return yn;
}

void Biquad::setCoefficients(std::array<double, ncoeffs> *coefficients) {
    memcpy(&coeffs, coefficients, sizeof(coeffs[a0]) * coeffs.size());
}

std::array<double, ncoeffs> *Biquad::getCoefficients() {
    return &coeffs;
}

std::array<double, nstates> *Biquad::getState() {
    return &state;
}

double Biquad::processDirect(double xn) {
    double yn =   (coeffs[a0] * xn)
                + (coeffs[a1] * state[x_z1])
                + (coeffs[a2] * state[x_z2])
                - (coeffs[b1] * state[y_z1])
                - (coeffs[b2] * state[y_z2]);

    handleUnderflow(&yn);

    state[x_z2] = state[x_z1];
    state[x_z1] = xn;

    state[y_z2] = state[y_z1];
    state[y_z1] = yn;

    return yn;
}

double Biquad::processCanonical(double xn) {
    double wn = xn - (coeffs[b1] * state[x_z1]) - (coeffs[b2] * state[x_z2]);
    handleUnderflow(&wn);

    double yn = (coeffs[a0] * wn) + (coeffs[a1] * state[x_z1]) + (coeffs[a2] * state[x_z2]);
    handleUnderflow(&yn);

    state[x_z2] = state[x_z1];
    state[x_z1] = wn;

    return yn;
}

double Biquad::processTransposeDirect(double xn) {
    double wn = xn + state[y_z1];
    handleUnderflow(&wn);
    double yn = (coeffs[a0] * wn) + state[x_z1];
    handleUnderflow(&yn);

    state[y_z1] = state[y_z2] - coeffs[b1] * wn;
    state[y_z2] = -coeffs[b2] * wn;

    state[x_z1] = state[x_z2] + coeffs[a1] * wn;
    state[x_z2] = coeffs[a2] * wn;

    return yn;
}

double Biquad::processTransposeCanonical(double xn) {
    double yn = coeffs[a0] * xn + state[x_z1];
    handleUnderflow(&yn);

    state[x_z1] = (coeffs[a1] * xn) - (coeffs[b1] * yn) + state[x_z2];
    state[x_z2] = (coeffs[a2] * xn) - (coeffs[b2] * yn);

    return yn;
}

void Biquad::handleUnderflow(double *value) {
    double smallest_pos_val = 1.175494351e-38;
    double smallest_neg_val = -1.175494351e-38;

    if ((*value > 0.0 && *value < smallest_pos_val) ||
        (*value < 0.0 && *value > smallest_neg_val)) {
        *value = 0;
    }
}
