#ifndef __AUDIO_FILTER_H_
#define __AUDIO_FILTER_H_

#include <array>

#include <biquad.h>

enum class FilterAlgorithm {
    LPF1P, LPF1, HPF1, LPF2, HPF2, BPF2, BSF2,
    ButterLPF2, ButterHPF2, ButterBPF2, ButterBSF2,
    MMALPF2, MMALPF2B,
    LowShelf, HiShelf,
    NCQParaEQ, CQParaEQ,
    LWRLPF2, LWRHPF2,
    APF1, APF2,
    ResonA, ResonB,
    MatchLP2A, MatchLP2B, MatchBP2A, MatchBP2B,
    ImpInvLP1, ImpInvLP2
};

struct AudioFilterParameters {
    AudioFilterParameters () {}
    AudioFilterParameters& operator=(const AudioFilterParameters &params) {
        if (this == &params) {
            return *this;
        }

        algorithm   = params.algorithm;
        fc          = params.fc;
        Q           = params.Q;
        boostcut_db = params.boostcut_db;

        return *this;
    }

    FilterAlgorithm algorithm = FilterAlgorithm::LPF1P;
    double fc = 1000.0;
    double Q = 0.707;
    double boostcut_db = 0.0;
};

class AudioFilter {
public:
    AudioFilter(Biquad *biquad, double sample_rate);
    ~AudioFilter();

    AudioFilterParameters getParameters();
    void setParameters(AudioFilterParameters params);
    bool reset(double sample_rate);
    double process(double xn);
    void setSampleRate(double sample_rate);

private:
    Biquad *biquad;
    double sample_rate;

    AudioFilterParameters parameters;

    std::array<double, ncoeffs> coeffs;

    void calculateCoefficients();
    void calculateLPF1P();
    void calculateLPF1();
    void calculateHPF1();
    void calculateLPF2();
    void calculateHPF2();
    void calculateBPF2();
    void calculateBSF2();
    void calculateButterLPF2();
    void calculateButterHPF2();
    void calculateButterBPF2();
    void calculateButterBSF2();
    void calculateMMALPF2();
    void calculateMMALPF2B();
    void calculateLowShelf();
    void calculateHiShelf();
    void calculateNCQParaEQ();
    void calculateCQParaEQ();
    void calculateLWRLPF2();
    void calculateLWRHPF2();
    void calculateAPF1();
    void calculateAPF2();
    void calculateResonA();
    void calculateResonB();
    void calculateMatchLP2A();
    void calculateMatchLP2B();
    void calculateMatchBP2A();
    void calculateMatchBP2B();
    void calculateImpInvLP1();
    void calculateImpInvLP2();
};

#endif // __AUDIO_FILTER_H_
