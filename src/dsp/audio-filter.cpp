#include <cstdio>
#include <cstring>
#include <cmath>

#include <audio-filter.h>

AudioFilter::AudioFilter(Biquad *biquad, double sample_rate) :
    biquad(biquad),
    sample_rate(sample_rate) {
    memset(&coeffs, 0, sizeof(coeffs[a0]) * coeffs.size());
    calculateCoefficients();
}

AudioFilter::~AudioFilter() {}

AudioFilterParameters AudioFilter::getParameters() {
    return parameters;
}

void AudioFilter::setParameters(AudioFilterParameters params) {
    parameters = params;
}

bool AudioFilter::reset(double sample_rate) {
    this->sample_rate = sample_rate;
    memset(&coeffs, 0, sizeof(coeffs[a0]) * coeffs.size());
    calculateCoefficients();

    return true;
}

double AudioFilter::process(double xn) {
    calculateCoefficients();
    double yn = (coeffs[d0] * xn) + (coeffs[c0] * biquad->process(xn));

    return yn;
}

void AudioFilter::setSampleRate(double sample_rate) {
    this->sample_rate;
}

void AudioFilter::calculateCoefficients() {
    // clear coeffs
    memset(&coeffs, 0, sizeof(coeffs[a0]) * coeffs.size());

    // set default pass-through values
    coeffs[a0] = 1.0f;
    coeffs[c0] = 1.0f;
    coeffs[d0] = 0.0f;

    switch (parameters.algorithm) {
    case FilterAlgorithm::LPF1P:
        calculateLPF1P();
        break;
    case FilterAlgorithm::LPF1:
        calculateLPF1();
        break;
    case FilterAlgorithm::HPF1:
        calculateHPF1();
        break;
    case FilterAlgorithm::LPF2:
        calculateLPF2();
        break;
    case FilterAlgorithm::HPF2:
        calculateHPF2();
        break;
    case FilterAlgorithm::BPF2:
        calculateBPF2();
        break;
    case FilterAlgorithm::BSF2:
        calculateBSF2();
        break;
    case FilterAlgorithm::ButterLPF2:
        calculateButterLPF2();
        break;
    case FilterAlgorithm::ButterHPF2:
        calculateButterHPF2();
        break;
    case FilterAlgorithm::ButterBPF2:
        calculateButterBPF2();
        break;
    case FilterAlgorithm::ButterBSF2:
        calculateButterBSF2();
        break;
    case FilterAlgorithm::MMALPF2:
        calculateMMALPF2();
        break;
    case FilterAlgorithm::MMALPF2B:
        calculateMMALPF2B();
        break;
    case FilterAlgorithm::LowShelf:
        calculateLowShelf();
        break;
    case FilterAlgorithm::HiShelf:
        calculateHiShelf();
        break;
    case FilterAlgorithm::NCQParaEQ:
        calculateNCQParaEQ();
        break;
    case FilterAlgorithm::CQParaEQ:
        calculateCQParaEQ();
        break;
    case FilterAlgorithm::LWRLPF2:
        calculateLWRLPF2();
        break;
    case FilterAlgorithm::LWRHPF2:
        calculateLWRHPF2();
        break;
    case FilterAlgorithm::APF1:
        calculateAPF1();
        break;
    case FilterAlgorithm::APF2:
        calculateAPF2();
        break;
    case FilterAlgorithm::ResonA:
        calculateResonA();
        break;
    case FilterAlgorithm::ResonB:
        calculateResonB();
        break;
    case FilterAlgorithm::MatchLP2A:
        calculateMatchLP2A();
        break;
    case FilterAlgorithm::MatchLP2B:
        calculateMatchLP2B();
        break;
    case FilterAlgorithm::MatchBP2A:
        calculateMatchBP2A();
        break;
    case FilterAlgorithm::MatchBP2B:
        calculateMatchBP2B();
        break;
    case FilterAlgorithm::ImpInvLP1:
        calculateImpInvLP1();
        break;
    case FilterAlgorithm::ImpInvLP2:
        calculateImpInvLP2();
        break;
    }

    biquad->setCoefficients(&coeffs);
}

void AudioFilter::calculateLPF1P() {
    double theta_c = (2.0 * M_PI * parameters.fc) / sample_rate;
    double gamma = 2.0 - cos(theta_c);

    double filter_b1 = pow((gamma * (gamma - 1.0)), 0.5) - gamma;
    double filter_a0 = 1.0 + filter_b1;

    // --- update coeffs
    coeffs[a0] = filter_a0;
    coeffs[a1] = 0.0;
    coeffs[a2] = 0.0;

    coeffs[b1] = filter_b1;
    coeffs[b2] = 0.0;
}

void AudioFilter::calculateLPF1() {
    double theta_c = (2.0 * M_PI * parameters.fc) / sample_rate;
    double gamma = cos(theta_c) / (1.0 + sin(theta_c));

    coeffs[a0] = (1.0 - gamma) / 2.0;
    coeffs[a1] = (1.0 - gamma) / 2.0;
    coeffs[a2] = 0.0;
    coeffs[b1] = -gamma;
    coeffs[b2] = 0.0;
}

void AudioFilter::calculateHPF1() {
    double theta_c = (2.0 * M_PI * parameters.fc) / sample_rate;
    double gamma = cos(theta_c) / (1.0 + sin(theta_c));

    coeffs[a0] = (1.0 + gamma) / 2.0;
    coeffs[a1] = -(1.0 + gamma) / 2.0;
    coeffs[a2] = 0.0;
    coeffs[b1] = -gamma;
    coeffs[b2] = 0.0;
}

void AudioFilter::calculateLPF2() {
    double theta_c = (2.0 * M_PI * parameters.fc) / sample_rate;
    double d = 1.0 / parameters.Q;
    double beta_numerator = 1.0 - ((d / 2.0) * sin(theta_c));
    double beta_denominator = 1.0 + ((d / 2.0) * sin(theta_c));

    double beta = 0.5 * (beta_numerator / beta_denominator);
    double gamma = (0.5 + beta) * cos(theta_c);
    double alpha = (0.5 + (beta - gamma)) / 2.0;

    coeffs[a0] = alpha;
    coeffs[a1] = alpha * 2.0;
    coeffs[a2] = alpha;
    coeffs[b1] = gamma * -2.0;
    coeffs[b2] = beta * 2.0;
}

void AudioFilter::calculateHPF2() {
    double theta_c = (2.0 * M_PI * parameters.fc) / sample_rate;
    double d = 1.0 / parameters.Q;

    double beta_numerator = 1.0 - ((d / 2.0) * sin(theta_c));
    double beta_denominator = 1.0 + ((d / 2.0) * sin(theta_c));

    double beta = 0.5 * (beta_numerator / beta_denominator);
    double gamma = (0.5 + beta) * cos(theta_c);
    double alpha = (0.5 + beta + gamma) / 2.0;

    coeffs[a0] = alpha;
    coeffs[a1] = alpha * -2.0;
    coeffs[a2] = alpha;
    coeffs[b1] = gamma * -2.0;
    coeffs[b2] = beta * 2.0;
}

void AudioFilter::calculateBPF2() {
    double K = tan((M_PI * parameters.fc) / sample_rate);
    double delta = (pow(K, 2) * parameters.Q) + K + parameters.Q;

    coeffs[a0] = K / delta;
    coeffs[a1] = 0.0;
    coeffs[a2] = -K / delta;
    coeffs[b1] = (2.0 * parameters.Q * (pow(K, 2) - 1)) / delta;
    coeffs[b2] = ((pow(K, 2) * parameters.Q) - K + parameters.Q) / delta;
}

void AudioFilter::calculateBSF2() {
    double K = tan((M_PI * parameters.fc) / sample_rate);
    double delta = (pow(K, 2) * parameters.Q) + K + parameters.Q;

    coeffs[a0] = (parameters.Q * (1 + pow(K, 2))) / delta;
    coeffs[a1] = (2.0 * parameters.Q * (pow(K, 2) - 1)) / delta;
    coeffs[a2] = (parameters.Q * (1 + pow(K, 2))) / delta;
    coeffs[b1] = (2.0 * parameters.Q * (pow(K, 2) - 1)) / delta;
    coeffs[b2] = ((pow(K, 2) * parameters.Q) - K + parameters.Q) / delta;
}

void AudioFilter::calculateButterLPF2() {
    double theta_c = (M_PI * parameters.fc) / sample_rate;
    double C = 1.0 / tan(theta_c);

    coeffs[a0] = 1.0 / (1.0 + (sqrt(2) * C) + pow(C, 2));
    coeffs[a1] = 2.0 * coeffs[a0];
    coeffs[a2] = coeffs[a0];
    coeffs[b1] = 2.0 * coeffs[a0] * (1.0 - pow(C, 2));
    coeffs[b2] = coeffs[a0] * (1.0 - (sqrt(2) * C) + pow(C, 2));
}

void AudioFilter::calculateButterHPF2() {
    double theta_c = (M_PI * parameters.fc) / sample_rate;
    double C = tan(theta_c);

    coeffs[a0] = 1.0 / (1.0 + (sqrt(2) * C) + pow(C, 2));
    coeffs[a1] = -2.0 * coeffs[a0];
    coeffs[a2] = coeffs[a0];
    coeffs[b1] = 2.0 * coeffs[a0] * (pow(C, 2) - 1.0);
    coeffs[b2] = coeffs[a0] * (1.0 - (sqrt(2) * C) + pow(C, 2));
}

void AudioFilter::calculateButterBPF2() {
    double theta_c = (2.0 * M_PI * parameters.fc) / sample_rate;
    double BW = parameters.fc / parameters.Q;
    double delta_c = (M_PI * BW) / sample_rate;

    if (delta_c >= ((0.95 * M_PI) / 2.0)) {
        delta_c = (0.95 * M_PI) / 2.0;
    }

    double C = 1.0 / tan(delta_c);
    double D = 2.0 * cos(theta_c);

    coeffs[a0] = 1.0 / (1.0 + C);
    coeffs[a1] = 0.0;
    coeffs[a2] = -coeffs[a0];
    coeffs[b1] = -coeffs[a0] * (C * D);
    coeffs[b2] = coeffs[a0] * (C - 1.0);
}

void AudioFilter::calculateButterBSF2() {
    double theta_c = (2.0 * M_PI * parameters.fc) / sample_rate;
    double BW = parameters.fc / parameters.Q;
    double delta_c = (M_PI * BW) / sample_rate;

    if (delta_c >= ((0.95 * M_PI) / 2.0)) {
        delta_c = (0.95 * M_PI) / 2.0;
    }

    double C = tan(delta_c);
    double D = 2.0 * cos(theta_c);

    coeffs[a0] = 1.0 / (1.0 + C);
    coeffs[a1] = -coeffs[a0] * D;
    coeffs[a2] = coeffs[a0];
    coeffs[b1] = -coeffs[a0] * D;
    coeffs[b2] = coeffs[a0] * (1.0 - C);
}

void AudioFilter::calculateMMALPF2() {
    double theta_c = (2.0 * M_PI * parameters.fc) / sample_rate;
    double resonance_db = 0.0;

    if (parameters.Q > 0.707) {
        double peak = pow(parameters.Q, 2) / sqrt(pow(parameters.Q, 2) - 0.25);
        resonance_db = 20.0 * log10(peak);
    }

    double resonance = cos(theta_c);
    resonance += sin(theta_c) * sqrt(pow(10, resonance_db / 10) - 1);
    resonance /= (pow(10, resonance_db / 20) * sin(theta_c)) + 1;
    double g = pow(10, -resonance_db / 40);

    double filter_b1 = -2.0 * resonance * cos(theta_c);
    double filter_b2 = pow(resonance, 2);
    double filter_a0 = g * (1 + filter_b1 + filter_b2);

    coeffs[a0] = filter_a0;
    coeffs[a1] = 0.0;
    coeffs[a2] = 0.0;
    coeffs[b1] = filter_b1;
    coeffs[b2] = filter_b2;
}

void AudioFilter::calculateMMALPF2B() {
    double theta_c = (2.0 * M_PI * parameters.fc) / sample_rate;
    double resonance_db = 0.0;

    if (parameters.Q > 0.707) {
        double peak = pow(parameters.Q, 2) / sqrt(pow(parameters.Q, 2) - 0.25);
        resonance_db = 20.0 * log10(peak);
    }

    double resonance = cos(theta_c);
    resonance += sin(theta_c) * sqrt(pow(10, resonance_db / 10) - 1);
    resonance /= (pow(10, resonance_db / 20) * sin(theta_c)) + 1;
    double g = 1.0;

    double filter_b1 = -2.0 * resonance * cos(theta_c);
    double filter_b2 = pow(resonance, 2);
    double filter_a0 = g * (1 + filter_b1 + filter_b2);

    coeffs[a0] = filter_a0;
    coeffs[a1] = 0.0;
    coeffs[a2] = 0.0;
    coeffs[b1] = filter_b1;
    coeffs[b2] = filter_b2;
}

void AudioFilter::calculateLowShelf() {
    double theta_c = (2.0 * M_PI * parameters.fc) / sample_rate;
    double mu = pow(10.0, parameters.boostcut_db / 20.0);

    double beta = 4.0 / (1.0 + mu);
    double delta = beta * tan(theta_c / 2.0);
    double gamma = (1.0 - delta) / (1.0 + delta);

    coeffs[a0] = (1.0 - gamma) / 2.0;
    coeffs[a1] = (1.0 - gamma) / 2.0;
    coeffs[a2] = 0.0;
    coeffs[b1] = -gamma;
    coeffs[b2] = 0.0;

    coeffs[c0] = mu - 1.0;
    coeffs[d0] = 1.0;
}

void AudioFilter::calculateHiShelf() {
    double theta_c = (2.0 * M_PI * parameters.fc) / sample_rate;
    double mu = pow(10.0, parameters.boostcut_db / 20.0);

    double beta = (1.0 + mu) / 4.0;
    double delta = beta * tan(theta_c / 2.0);
    double gamma = (1.0 - delta) / (1.0 + delta);

    coeffs[a0] = (1.0 + gamma) / 2.0;
    coeffs[a1] = -coeffs[a0];
    coeffs[a2] = 0.0;
    coeffs[b1] = -gamma;
    coeffs[b2] = 0.0;

    coeffs[c0] = mu - 1.0;
    coeffs[d0] = 1.0;
}

void AudioFilter::calculateNCQParaEQ() {
    double theta_c = (2.0 * M_PI * parameters.fc) / sample_rate;
    double mu = pow(10.0, parameters.boostcut_db / 20.0);

    double zeta = 4.0 / (1.0 + mu);

    double beta_numerator = 1.0 - (zeta * tan(theta_c / (parameters.Q * 2.0)));
    double beta_denominator = 1.0 + (zeta * tan(theta_c / (parameters.Q * 2.0)));
    double beta = 0.5 * (beta_numerator / beta_denominator);

    double gamma = (0.5 + beta) * cos(theta_c);
    double alpha = 0.5 - beta;

    coeffs[a0] = alpha;
    coeffs[a1] = 0.0;
    coeffs[a2] = -alpha;
    coeffs[b1] = gamma * -2.0;
    coeffs[b2] = beta * 2.0;

    coeffs[c0] = mu - 1.0;
    coeffs[d0] = 1.0;
}

void AudioFilter::calculateCQParaEQ() {
    bool boost = parameters.boostcut_db >= 0;

    double K = tan((M_PI * parameters.fc) / sample_rate);
    double Vo = pow(10.0, (parameters.boostcut_db / 20.0));

    double d = 1.0 + ((1.0 / parameters.Q) * K) + pow(K, 2);
    double e = 1.0 + ((1.0 / (Vo * parameters.Q)) * K) + pow(K, 2);

    double alpha = 1.0 + ((Vo / parameters.Q) * K) + pow(K, 2);
    double beta = 2.0 * (pow(K, 2) - 1.0);
    double gamma = 1 - ((Vo / parameters.Q) * K) + pow(K, 2);
    double delta = 1 - ((1.0 / parameters.Q) * K) + pow(K, 2);
    double eta = 1 - ((1.0 / (Vo * parameters.Q)) * K) + pow(K, 2);

    if (boost) {
        coeffs[a0] = alpha / d;
        coeffs[a1] = beta / d;
        coeffs[a2] = gamma / d;
        coeffs[b1] = beta / d;
        coeffs[b2] = delta / d;
    } else {
        coeffs[a0] = d / e;
        coeffs[a1] = beta / e;
        coeffs[a2] = delta / e;
        coeffs[b1] = beta / e;
        coeffs[b2] = eta / e;
    }

    coeffs[c0] = 1.0;
    coeffs[d0] = 0.0;
}

void AudioFilter::calculateLWRLPF2() {
    double omega_c = M_PI * parameters.fc;
    double theta_c = omega_c / sample_rate;

    double kappa = omega_c / tan(theta_c);
    double delta = pow(kappa, 2) + pow(omega_c, 2) + (2.0 * kappa * omega_c);

    coeffs[a0] = pow(omega_c, 2) / delta;
    coeffs[a1] = 2.0 * coeffs[a0];
    coeffs[a2] = coeffs[a0];
    coeffs[b1] = ((-2.0 * pow(kappa, 2)) + (2.0 * pow(omega_c, 2))) / delta;
    coeffs[b2] = ((-2.0 * kappa * omega_c) + pow(kappa, 2) + pow(omega_c, 2)) / delta;

    coeffs[c0] = 1.0;
    coeffs[d0] = 0.0;
}

void AudioFilter::calculateLWRHPF2() {
    double omega = M_PI * parameters.fc;
    double theta = omega / sample_rate;

    double kappa = omega / tan(theta);
    double delta = pow(kappa, 2) + pow(omega, 2) + (2.0 * kappa * omega);

    coeffs[a0] = pow(kappa, 2) / delta;
    coeffs[a1] = (-2.0 * pow(kappa, 2)) / delta;
    coeffs[a2] = coeffs[a0];
    coeffs[b1] = ((-2.0 * pow(kappa, 2)) + (2.0 * pow(omega, 2))) / delta;
    coeffs[b2] = ((-2.0 * kappa * omega) + pow(kappa, 2) + pow(omega, 2)) / delta;

    coeffs[c0] = 1.0;
    coeffs[d0] = 0.0;
}

void AudioFilter::calculateAPF1() {
    double theta = (M_PI * parameters.fc) / sample_rate;
    double alpha = (tan(theta) - 1.0) / (tan(theta) + 1.0);

    coeffs[a0] = alpha;
    coeffs[a1] = 1.0;
    coeffs[a2] = 0.0;
    coeffs[b1] = alpha;
    coeffs[b2] = 0.0;

    coeffs[c0] = 1.0;
    coeffs[d0] = 0.0;
}

void AudioFilter::calculateAPF2() {
    double BW = parameters.fc / parameters.Q;
    double theta = (BW * M_PI) / sample_rate;

    double alpha = (tan(theta) - 1.0) / (tan(theta) + 1.0);

    theta = (2.0 * M_PI * parameters.fc) / sample_rate;
    double beta = -cos(theta);

    coeffs[a0] = -alpha;
    coeffs[a1] = beta * (1.0 - alpha);
    coeffs[a2] = 1.0;
    coeffs[b1] = beta * (1.0 - alpha);
    coeffs[b2] = -alpha;

    coeffs[c0] = 1.0;
    coeffs[d0] = 0.0;
}

void AudioFilter::calculateResonA() {
    double theta = 2.0 * M_PI * parameters.fc / sample_rate;
    double BW = parameters.fc / parameters.Q;
    double filter_b2 = exp(-2.0*M_PI*(BW / sample_rate));
    double filter_b1 = ((-4.0*filter_b2) / (1.0 + filter_b2))*cos(theta);
    double filter_a0 = (1.0 - filter_b2)*pow((1.0 - (filter_b1*filter_b1) / (4.0 * filter_b2)), 0.5);

    coeffs[a0] = filter_a0;
    coeffs[a1] = 0.0;
    coeffs[a2] = 0.0;
    coeffs[b1] = filter_b1;
    coeffs[b2] = filter_b2;
}

void AudioFilter::calculateResonB() {
    double theta = (2.0 * M_PI * parameters.fc) / sample_rate;
    double BW = parameters.fc / parameters.Q;
    double filter_b2 = exp(-2.0 * M_PI * (BW / sample_rate));
    double filter_b1 = ((-4.0 * filter_b2) / (1.0 + filter_b2)) * cos(theta);
    double filter_a0 = 1.0 - pow(filter_b2, 0.5); // (1.0 - filter_b2)*pow((1.0 - (filter_b1*filter_b1) / (4.0 * filter_b2)), 0.5);

    coeffs[a0] = filter_a0;
    coeffs[a1] = 0.0;
    coeffs[a2] = -filter_a0;
    coeffs[b1] = filter_b1;
    coeffs[b2] = filter_b2;
}

void AudioFilter::calculateMatchLP2A() {
    double theta = 2.0 * M_PI * parameters.fc / sample_rate;

    double q = 1.0 / (2.0 * parameters.Q);

    // --- impulse invariant
    double b_1 = 0.0;
    double b_2 = exp(-2.0 * q * theta);
    if (q <= 1.0) {
        b_1 = -2.0 * exp(-q * theta) * cos(pow((1.0 - q*q), 0.5) * theta);
    } else {
        b_1 = -2.0 * exp(-q * theta) * cosh(pow((q*q - 1.0), 0.5) * theta);
    }

    // --- TIGHT FIT --- //
    double B0 = (1.0 + b_1 + b_2)*(1.0 + b_1 + b_2);
    double B1 = (1.0 - b_1 + b_2)*(1.0 - b_1 + b_2);
    double B2 = -4.0*b_2;

    double phi_0 = 1.0 - sin(theta / 2.0)*sin(theta / 2.0);
    double phi_1 = sin(theta / 2.0)*sin(theta / 2.0);
    double phi_2 = 4.0*phi_0*phi_1;

    double R1 = (B0*phi_0 + B1*phi_1 + B2*phi_2)*(parameters.Q*parameters.Q);
    double A0 = B0;
    double A1 = (R1 - A0*phi_0) / phi_1;

    if (A0 < 0.0) A0 = 0.0;
    if (A1 < 0.0) A1 = 0.0;

    double a_0 = 0.5*(pow(A0, 0.5) + pow(A1, 0.5));
    double a_1 = pow(A0, 0.5) - a_0;
    double a_2 = 0.0;

    coeffs[a0] = a_0;
    coeffs[a1] = a_1;
    coeffs[a2] = a_2;
    coeffs[b1] = b_1;
    coeffs[b2] = b_2;
}

void AudioFilter::calculateMatchLP2B() {
    double theta = 2.0 * M_PI * parameters.fc / sample_rate;
    double q = 1.0 / (2.0 * parameters.Q);

    // --- impulse invariant
    double b_1 = 0.0;
    double b_2 = exp(-2.0 * q * theta);
    if (q <= 1.0) {
        b_1 = -2.0 * exp(-q * theta) * cos(pow((1.0 - q*q), 0.5) * theta);
    } else {
        b_1 = -2.0 * exp(-q * theta) * cosh(pow((q*q - 1.0), 0.5) * theta);
    }

    // --- LOOSE FIT --- //
    double f0 = theta / M_PI; // note f0 = fraction of pi, so that f0 = 1.0 = pi = Nyquist

    double r0 = 1.0 + b_1 + b_2;
    double denom = (1.0 - f0*f0)*(1.0 - f0*f0) + (f0*f0) / (parameters.Q*parameters.Q);
    denom = pow(denom, 0.5);
    double r1 = ((1.0 - b_1 + b_2)*f0*f0) / (denom);

    double a_0 = (r0 + r1) / 2.0;
    double a_1 = r0 - a_0;
    double a_2 = 0.0;

    coeffs[a0] = a_0;
    coeffs[a1] = a_1;
    coeffs[a2] = a_2;
    coeffs[b1] = b_1;
    coeffs[b2] = b_2;
}

void AudioFilter::calculateMatchBP2A() {
    double theta = 2.0 * M_PI * parameters.fc / sample_rate;
    double q = 1.0 / (2.0 * parameters.Q);

    // --- impulse invariant
    double b_1 = 0.0;
    double b_2 = exp(-2.0 * q * theta);
    if (q <= 1.0) {
        b_1 = -2.0 * exp(-q * theta) * cos(pow((1.0 - q*q), 0.5) * theta);
    } else {
        b_1 = -2.0 * exp(-q * theta) * cosh(pow((q*q - 1.0), 0.5) * theta);
    }

    // --- TIGHT FIT --- //
    double B0 = (1.0 + b_1 + b_2)*(1.0 + b_1 + b_2);
    double B1 = (1.0 - b_1 + b_2)*(1.0 - b_1 + b_2);
    double B2 = -4.0*b_2;

    double phi_0 = 1.0 - sin(theta / 2.0)*sin(theta / 2.0);
    double phi_1 = sin(theta / 2.0)*sin(theta / 2.0);
    double phi_2 = 4.0*phi_0*phi_1;

    double R1 = (B0*phi_0 + B1*phi_1 + B2*phi_2);
    double R2 = -B0 + B1 + (4.0 * (phi_0 - phi_1) * B2);
    double A2 = (R1 - (R2 * phi_1)) / (4 * phi_1 * phi_1);
    double A1 = R2 + (4 * (phi_1 - phi_0) * A2);

    double a_1 = -0.5 * pow(A1, 0.5);
    double a_0 = 0.5 * (pow(A2 + pow(a_1, 2), 0.5) - a_1);
    double a_2 = -a_0 - a_1; 

    coeffs[a0] = a_0;
    coeffs[a1] = a_1;
    coeffs[a2] = a_2;
    coeffs[b1] = b_1;
    coeffs[b2] = b_2;
}

void AudioFilter::calculateMatchBP2B() {
    double theta = 2.0 * M_PI * parameters.fc / sample_rate;
    double q = 1.0 / (2.0 * parameters.Q);

    // --- impulse invariant
    double b_1 = 0.0;
    double b_2 = exp(-2.0 * q * theta);
    if (q <= 1.0) {
        b_1 = -2.0 * exp(-q * theta) * cos(pow((1.0 - q*q), 0.5) * theta);
    } else {
        b_1 = -2.0 * exp(-q * theta) * cosh(pow((q*q - 1.0), 0.5) * theta);
    }

    double f0 = theta / M_PI;
    double r0 = (1.0 + b_1 + b_2) / (M_PI * f0 * parameters.Q);

    double r1_numer = (1.0 - b_1 + b_2) * (f0 / parameters.Q);
    double r1_denom = pow(1 - pow(f0, 2), 2) + (pow(f0, 2) / pow(parameters.Q, 2));
    r1_denom = pow(r1_denom, 0.5);

    double r1 = r1_numer / r1_denom;

    double a_1 = -r1 / 2.0;
    double a_0 = (r0 - a_1) / 2.0;
    double a_2 = -a_0 - a_1;

    coeffs[a0] = a_0;
    coeffs[a1] = a_1;
    coeffs[a2] = a_2;
    coeffs[b1] = b_1;
    coeffs[b2] = b_2;
}

void AudioFilter::calculateImpInvLP1() {
    double T = 1.0 / sample_rate;
    double omega = 2.0 * M_PI * parameters.fc;

    double a_0 = 1 - exp(-omega * T);
    double b_1 = -exp(-omega * T);

    coeffs[a0] = a_0;
    coeffs[b1] = b_1;
}

void AudioFilter::calculateImpInvLP2() {
    double omega = (2.0 * M_PI * parameters.fc) / sample_rate;

    double Pre = -omega / (2.0 * parameters.Q);
    double Pim = omega * (pow(1 - pow(1 / (2.0 * parameters.Q), 2), 0.5));
    double Cre = 0;
    double Cim = omega / (2.0 * (pow(1 - pow(1 / (2.0 * parameters.Q), 2), 0.5)));

    double T = 1.0;
    double a_0 = Cre;
    double a_1 = -2.0 * ((Cre * cos(Pim)) + (Cim * sin(Pim))) * exp(Pre);
    double a_2 = 0;
    double b_1 = -2.0 * cos(Pim) * exp(Pre);
    double b_2 = pow(exp(Pre), 2);

    coeffs[a0] = a_0;
    coeffs[a1] = a_1;
    coeffs[a2] = a_2;
    coeffs[b1] = b_1;
    coeffs[b2] = b_2;
}
