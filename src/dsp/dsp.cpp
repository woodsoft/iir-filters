#include <cmath>
#include <dsp.h>

DSP::DSP(Params *params, double sample_rate) :
    params(params),
    sample_rate(sample_rate) {
    biquad = new Biquad(sample_rate);
    audio_filter = new AudioFilter(biquad, sample_rate);
}

DSP::~DSP() {
    delete audio_filter;
    delete biquad;
}

double DSP::process(double in) {
    updateVolume();

    double out = audio_filter->process(in) * volume;

    return out;
}

AudioFilter *DSP::getAudioFilter() {
    return audio_filter;
}

Biquad *DSP::getBiquad() {
    return biquad;
}

void DSP::updateVolume() {
    param_data_t *volume_data = params->getParamData(P_VOLUME);
    volume = pow(10, (volume_data->value / 20));
}
