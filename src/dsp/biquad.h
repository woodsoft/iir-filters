#ifndef __BIQUAD_H_
#define __BIQUAD_H_

#include <array>

// coeffs array index values
enum { a0, a1, a2, b1, b2, c0, d0, ncoeffs };

// state array index values
enum { x_z1, x_z2, y_z1, y_z2, nstates };

// Calculation algorithm
enum class BiquadAlgorithm {
    Direct,
    Canonical,
    TransposeDirect,
    TransposeCanonical
};

struct BiquadParameters {
    BiquadParameters() {}
    BiquadParameters& operator=(const BiquadParameters &params) {
        return *this;
    }

    BiquadAlgorithm biquad_algorithm = BiquadAlgorithm::Direct;
};

class Biquad {
public:
    Biquad(double sample_rate);
    ~Biquad();

    BiquadParameters getParameters();
    void setParameters(BiquadParameters params);
    bool reset(double sample_rate);
    double process(double xn);
    void setCoefficients(std::array<double, ncoeffs> *coefficients);
    std::array<double, ncoeffs> *getCoefficients();
    std::array<double, nstates> *getState();

private:
    double sample_rate;

    BiquadParameters biquad_parameters;
    std::array<double, ncoeffs> coeffs;
    std::array<double, nstates> state;

    double processDirect(double xn);
    double processCanonical(double xn);
    double processTransposeDirect(double xn);
    double processTransposeCanonical(double xn);

    void handleUnderflow(double *value);
};

#endif // __BIQUAD_H_
