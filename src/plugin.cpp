#include <cstdio>
#include <cstring>

#include <plugin.h>

static Plugin *getPlugin(const clap_plugin_t *cplugin) {
    return (Plugin *) cplugin->plugin_data;
}

Plugin::Plugin(const char *plugin_path, const clap_host_t *host) :
    plugin_path(plugin_path), host(host) {
    this->initClapPlugin();
}

bool Plugin::init() {
    fprintf(stderr, "Plugin::init()\n");
    // Initialize extensions so that their CLAP structures are ready for getExtension()
    params      = new Params();
    audio_ports = new AudioPorts();
    timer       = new Timer(host);
    state       = new State(params);
    gui         = new GUI(params);

    active = processing = false;

    return true;
}

void Plugin::destroy() {
    fprintf(stderr, "Plugin::destroy()\n");
    delete audio_ports;
    delete timer;
    delete state;
    delete gui;
    delete params;
}

bool Plugin::activate(double sample_rate, uint32_t min_nframes, uint32_t max_nframes) {
    fprintf(stderr, "Plugin::activate()\n");
    dsp = new DSP(params, sample_rate);
    this->sample_rate = sample_rate;
    this->min_nframes = min_nframes;
    this->max_nframes = max_nframes;

    active = true;

    return true;
}

void Plugin::deactivate() {
    fprintf(stderr, "Plugin::deactivate()\n");
    this->sample_rate = 0.0f;
    this->min_nframes = 0;
    this->max_nframes = 0;
    delete dsp;

    active = false;
}

bool Plugin::startProcessing() {
    fprintf(stderr, "Plugin::startProcessing()\n");
    processing = true;
    return true;
}

void Plugin::stopProcessing() {
    fprintf(stderr, "Plugin::stopProcessing()\n");
    processing = false;
}

void Plugin::reset() {
    fprintf(stderr, "Plugin::reset()\n");
}

clap_process_status Plugin::process(const clap_process_t *process) {
    static bool printed = false;

    if (!printed) {
        fprintf(stderr, "Plugin::process()\n");
        printed = true;
    }
    uint32_t num_frames = process->frames_count;
    uint32_t num_events = process->in_events->size(process->in_events);
    uint32_t event_idx = 0;
    uint32_t next_event_frame = num_events > 0 ? 0 : num_frames;

    this->params->syncMainToAudio(process->out_events);

    for (uint32_t i = 0; i < num_frames; ) {
        while (event_idx < num_events && next_event_frame == i) {
            const clap_event_header_t *event = process->in_events->get(process->in_events, event_idx);

            if (event->time != i) {
                next_event_frame = event->time;
                break;
            }

            this->processEvent(event);
            event_idx++;

            if (event_idx == num_events) {
                next_event_frame = num_frames;
                break;
            }
        }

        // TODO render any audio from instruments here

        for (; i < next_event_frame; i++) {
            // grab input samples
            const float in_l = process->audio_inputs[0].data32[0][i];
            const float in_r = process->audio_inputs[0].data32[1][i];

            // process samples
            float out_l = dsp->process(in_l);
            float out_r = dsp->process(in_r);

            // write output samples
            process->audio_outputs[0].data32[0][i] = out_l;
            process->audio_outputs[0].data32[1][i] = out_r;
        }
    }

    return CLAP_PROCESS_CONTINUE;
}

const void* Plugin::getExtension(const char *id) {
    fprintf(stderr, "Plugin::getExtension()\n");
    if (!strcmp(id, CLAP_EXT_AUDIO_PORTS))      return audio_ports->clapExtension();
    if (!strcmp(id, CLAP_EXT_TIMER_SUPPORT))    return timer->clapExtension();
    if (!strcmp(id, CLAP_EXT_PARAMS))           return params->clapExtension();
    if (!strcmp(id, CLAP_EXT_STATE))            return state->clapExtension();
    if (!strcmp(id, CLAP_EXT_GUI))              return gui->clapExtension();
    return nullptr;
}

void Plugin::onMainThread() {
    fprintf(stderr, "Plugin::onMainThread()\n");
}

const clap_plugin_t* Plugin::clapPlugin() {
    return &plugin;
}

AudioPorts *Plugin::getAudioPorts() {
    return audio_ports;
}

Timer *Plugin::getTimer() {
    return timer;
}

Params *Plugin::getParams() {
    return params;
}

State *Plugin::getState() {
    return state;
}

GUI *Plugin::getGUI() {
    return gui;
}

DSP *Plugin::getDSP() {
    return dsp;
}

void Plugin::processEvent(const clap_event_header_t *event) {
    if (event->space_id != CLAP_CORE_EVENT_SPACE_ID) return;

    switch (event->type) {
    case CLAP_EVENT_PARAM_VALUE:
        params->processEvent(event);
        break;
    }
}

void Plugin::startTimer() {
    timer->createTimer(TIMER_MS, &this->timer_id);
}

void Plugin::stopTimer() {
    timer->destroyTimer(&this->timer_id);
}

void Plugin::onTimer(clap_id timer_id) {
    if (timer_id == this->timer_id) {
        gui->show(dsp);
    }
}

void Plugin::initClapPlugin() {
    plugin = {
        .desc = &DESCRIPTOR,
        .plugin_data = this,

        .init = [] (const clap_plugin_t *cplugin) -> bool {
            return getPlugin(cplugin)->init();
        },

        .destroy = [] (const clap_plugin_t *cplugin) {
            getPlugin(cplugin)->destroy();
        },

        .activate = [] (const clap_plugin_t *cplugin, double sample_rate, uint32_t min_nframes, uint32_t max_nframes) -> bool {
            return getPlugin(cplugin)->activate(sample_rate, min_nframes, max_nframes);
        },

        .deactivate = [] (const clap_plugin_t *cplugin) {
            getPlugin(cplugin)->deactivate();
        },

        .start_processing = [] (const clap_plugin_t *cplugin) -> bool {
            return getPlugin(cplugin)->startProcessing();
        },

        .stop_processing = [] (const clap_plugin_t *cplugin) {
            getPlugin(cplugin)->stopProcessing();
        },

        .reset = [] (const clap_plugin_t *cplugin) {
            getPlugin(cplugin)->reset();
        },

        .process = [] (const clap_plugin_t *cplugin, const clap_process_t *process) -> clap_process_status {
            return getPlugin(cplugin)->process(process);
        },

        .get_extension = [] (const clap_plugin_t *cplugin, const char *id) -> const void * {
            return getPlugin(cplugin)->getExtension(id);
        },

        .on_main_thread = [] (const clap_plugin_t *cplugin) {
            getPlugin(cplugin)->onMainThread();
        },
    };
}
