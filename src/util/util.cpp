#include <util.h>

void util::render_audio(MyPlugin *plugin, uint32_t start, uint32_t end, float *out_l, float *out_r) {
    for (uint32_t index = start; index < end; index++) {
        float sum = 0.0f;

        for (int i = 0; i < plugin->voices.Length(); i++) {
            Voice *voice = &plugin->voices[i];
            if (!voice->held) continue;

            // float volume = FloatClamp01(plugin->parameters[P_COUNT] + voice->parameterOffsets[P_VOLUME]);
            sum += sinf(voice->phase * 2.0f * 3.14159f) * 2.0f; // * volume;

            voice->phase += 440.0f * exp2f((voice->key - 57.0f) / 12.0f) / plugin->sampleRate;
            voice->phase -= floorf(voice->phase);
        }

        out_l[index] = sum;
        out_r[index] = sum;
    }
}

void util::process_event(MyPlugin *plugin, const clap_event_header_t *event) {
    if (event->space_id == CLAP_CORE_EVENT_SPACE_ID) {
        if (event->type == CLAP_EVENT_NOTE_ON || event->type == CLAP_EVENT_NOTE_OFF || event->type == CLAP_EVENT_NOTE_CHOKE) {
            const clap_event_note_t *noteEvent = (const clap_event_note_t *) event;

            // Look through our voices array, and if the event matches any of them, it must have been released.
            for (int i = 0; i < plugin->voices.Length(); i++) {
                Voice *voice = &plugin->voices[i];

                if ((noteEvent->key == -1 || voice->key == noteEvent->key)
                        && (noteEvent->note_id == -1 || voice->noteID == noteEvent->note_id)
                        && (noteEvent->channel == -1 || voice->channel == noteEvent->channel)) {
                    if (event->type == CLAP_EVENT_NOTE_CHOKE) {
                        // Stop the voice immediately; don't process the release segment of any ADSR envelopes.
                        plugin->voices.Delete(i--);
                    } else {
                        voice->held = false;
                    }
                }
            }

            // If this is a note on event, create a new voice and add it to our array.
            if (event->type == CLAP_EVENT_NOTE_ON) {
                Voice voice = {
                    .held = true,
                    .noteID = noteEvent->note_id,
                    .channel = noteEvent->channel,
                    .key = noteEvent->key,
                    .phase = 0.0f,
                };

                plugin->voices.Add(voice);
            }
        }

        if (event->type == CLAP_EVENT_PARAM_VALUE) {
            const clap_event_param_value_t *valueEvent = (const clap_event_param_value_t *) event;
            uint32_t i = (uint32_t) valueEvent->param_id;
            pthread_mutex_lock(&plugin->syncParameters);
            plugin->parameters[i] = valueEvent->value;
            plugin->changed[i] = true;
            pthread_mutex_unlock(&plugin->syncParameters);
        }

        if (event->type == CLAP_EVENT_PARAM_MOD) {
            const clap_event_param_mod_t *modEvent = (const clap_event_param_mod_t *) event;

            for (int i = 0; i < plugin->voices.Length(); i++) {
                Voice *voice = &plugin->voices[i];

                if ((modEvent->key == -1 || voice->key == modEvent->key)
                        && (modEvent->note_id == -1 || voice->noteID == modEvent->note_id)
                        && (modEvent->channel == -1 || voice->channel == modEvent->channel)) {
                    voice->parameterOffsets[modEvent->param_id] = modEvent->amount;
                    break;
                }
            }
        }
    }
}

void util::sync_main_to_audio(MyPlugin *plugin, const clap_output_events_t *out) {
    pthread_mutex_lock(&plugin->syncParameters);

    for (uint32_t i = 0; i < P_COUNT; i++) {
        if (plugin->mainChanged[i]) {
            plugin->parameters[i] = plugin->mainParameters[i];
            plugin->mainChanged[i] = false;

            clap_event_param_value_t event = {};
            event.header.size = sizeof(event);
            event.header.time = 0;
            event.header.space_id = CLAP_CORE_EVENT_SPACE_ID;
            event.header.type = CLAP_EVENT_PARAM_VALUE;
            event.header.flags = 0;
            event.param_id = i;
            event.cookie = NULL;
            event.note_id = -1;
            event.port_index = -1;
            event.channel = -1;
            event.key = -1;
            event.value = plugin->parameters[i];
            out->try_push(out, &event.header);
        }
    }

    pthread_mutex_unlock(&plugin->syncParameters);
}

bool util::sync_audio_to_main(MyPlugin *plugin) {
    bool anyChanged = false;
    pthread_mutex_lock(&plugin->syncParameters);

    for (uint32_t i = 0; i < P_COUNT; i++) {
        if (plugin->changed[i]) {
            plugin->mainParameters[i] = plugin->parameters[i];
            plugin->changed[i] = false;
            anyChanged = true;
        }
    }

    pthread_mutex_unlock(&plugin->syncParameters);
    return anyChanged;
}

unsigned int util::get_tick_count() {
    struct timeval tm = {0};
    gettimeofday(&tm, NULL);
    return (unsigned int)(tm.tv_sec*1000 + tm.tv_usec/1000);
}

float util::float_clamp(float x, float min, float max) {
    return x >= max ? max : x <= min ? min : x;
}

