#!/usr/bin/bash

git submodule update --init --recursive

./external/vcpkg/bootstrap-vcpkg.sh

./config.sh

./build.sh
